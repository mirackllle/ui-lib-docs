import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MuiButtonNavViewComponent } from './lib-view/mui-button-nav-view/mui-button-nav-view.component';
import { MuiButtonSecNavViewComponent } from './lib-view/mui-button-sec-nav-view/mui-button-sec-nav-view.component';
import { MuiButtonSecViewComponent } from './lib-view/mui-button-sec-view/mui-button-sec-view.component';
import { MuiButtonViewComponent } from './lib-view/mui-button-view/mui-button-view.component';
import { MuiCheckboxViewComponent } from './lib-view/mui-checkbox-view/mui-checkbox-view.component';
import { MuiIconViewComponent } from './lib-view/mui-icon-view/mui-icon-view.component';
import { MuiInputViewComponent } from './lib-view/mui-input-view/mui-input-view.component';
import { MuiNotifyViewComponent } from './lib-view/mui-notify-view/mui-notify-view.component';
import { MuiPaginatorViewComponent } from './lib-view/mui-paginator-view/mui-paginator-view.component';
import { MuiPermissionDirectiveViewComponent } from './lib-view/mui-permission-directive-view/mui-permission-directive-view.component';
import { MuiRangepickerViewComponent } from './lib-view/mui-rangepicker-view/mui-rangepicker-view.component';
import { MuiSelectViewComponent } from './lib-view/mui-select-view/mui-select-view.component';
import { MuiSnackBarViewComponent } from './lib-view/mui-snack-bar-view/mui-snack-bar-view.component';
import { MuiTabSwitcherViewComponent } from './lib-view/mui-tab-switcher-view/mui-tab-switcher-view.component';

const routes: Routes = [
  { path: 'mui-button', component: MuiButtonViewComponent },
  { path: 'mui-button-nav', component: MuiButtonNavViewComponent },
  { path: 'mui-button-sec', component: MuiButtonSecViewComponent },
  { path: 'mui-button-sec-nav', component: MuiButtonSecNavViewComponent },
  { path: 'mui-checkbox', component: MuiCheckboxViewComponent },
  { path: 'mui-icon', component: MuiIconViewComponent },
  { path: 'mui-input', component: MuiInputViewComponent },
  { path: 'mui-notify', component: MuiNotifyViewComponent },
  { path: 'mui-paginator', component: MuiPaginatorViewComponent },
  { path: 'mui-select', component: MuiSelectViewComponent },
  { path: 'mui-snack-bar', component: MuiSnackBarViewComponent },
  { path: 'mui-tab-switcher', component: MuiTabSwitcherViewComponent },
  { path: 'mui-permission-directive', component: MuiPermissionDirectiveViewComponent },
  { path: 'mui-rangepicker', component: MuiRangepickerViewComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
