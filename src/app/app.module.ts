import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MuiButtonViewComponent } from './lib-view/mui-button-view/mui-button-view.component';
import { MuiButtonNavViewComponent } from './lib-view/mui-button-nav-view/mui-button-nav-view.component';
import { MuiButtonSecNavViewComponent } from './lib-view/mui-button-sec-nav-view/mui-button-sec-nav-view.component';
import { MuiButtonSecViewComponent } from './lib-view/mui-button-sec-view/mui-button-sec-view.component';
import { MuiButtonComponent } from './lib/mui-button/mui-button.component';
import { MuiButtonNavComponent } from './lib/mui-button-nav/mui-button-nav.component';
import { MuiButtonSecComponent } from './lib/mui-button-sec/mui-button-sec.component';
import { MuiButtonSecNavComponent } from './lib/mui-button-sec-nav/mui-button-sec-nav.component';
import { MuiTabSwitcherComponent } from './lib/mui-tab-switcher/mui-tab-switcher.component';
import { MuiLabelComponent } from './lib/mui-label/mui-label.component';
import { MuiCheckboxViewComponent } from './lib-view/mui-checkbox-view/mui-checkbox-view.component';
import { MuiCheckboxComponent } from './lib/mui-checkbox/mui-checkbox.component';
import { MuiIconViewComponent } from './lib-view/mui-icon-view/mui-icon-view.component';
import { MuiInputViewComponent } from './lib-view/mui-input-view/mui-input-view.component';
import { MuiNotifyViewComponent } from './lib-view/mui-notify-view/mui-notify-view.component';
import { MuiPaginatorViewComponent } from './lib-view/mui-paginator-view/mui-paginator-view.component';
import { MuiSelectViewComponent } from './lib-view/mui-select-view/mui-select-view.component';
import { MuiSnackBarViewComponent } from './lib-view/mui-snack-bar-view/mui-snack-bar-view.component';
import { MuiTabSwitcherViewComponent } from './lib-view/mui-tab-switcher-view/mui-tab-switcher-view.component';
import { MuiPermissionDirectiveViewComponent } from './lib-view/mui-permission-directive-view/mui-permission-directive-view.component';
import { MuiInputComponent } from './lib/mui-input/mui-input.component';
import { MuiIconsComponent } from './lib/mui-icons/mui-icons.component';
import { MuiNotifyComponent } from './lib/mui-notify/mui-notify.component';
import { MuiPaginatorComponent } from './lib/mui-paginator/mui-paginator.component';
import { MuiSelectComponent } from './lib/mui-select/mui-select.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MuiOptionComponent } from './lib/mui-option/mui-option.component';
import { UiLibModule } from './lib/ui-lib.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MuiRangepickerViewComponent } from './lib-view/mui-rangepicker-view/mui-rangepicker-view.component';

@NgModule({
  declarations: [
    AppComponent,
    MuiButtonViewComponent,
    MuiButtonNavViewComponent,
    MuiButtonSecNavViewComponent,
    MuiButtonSecViewComponent,
    MuiCheckboxViewComponent,
    MuiIconViewComponent,
    MuiInputViewComponent,
    MuiNotifyViewComponent,
    MuiPaginatorViewComponent,
    MuiSelectViewComponent,
    MuiSnackBarViewComponent,
    MuiTabSwitcherViewComponent,
    MuiPermissionDirectiveViewComponent,
    MuiRangepickerViewComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    UiLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
