import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiCheckboxComponent } from './mui-checkbox.component';

describe('muiCheckboxComponent', () => {
  let component: MuiCheckboxComponent;
  let fixture: ComponentFixture<MuiCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuiCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
