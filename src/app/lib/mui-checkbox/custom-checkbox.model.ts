export class CustomCheckBoxModel {
  icon?: string;
  type?: string;

  constructor(icon?: string, type?: string) {
    this.icon = icon;
    this.type = 'accent';
  }
}
