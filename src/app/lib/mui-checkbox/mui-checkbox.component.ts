import {Component, Input, forwardRef, HostBinding} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'mui-checkbox',
  templateUrl: './mui-checkbox.component.html',
  styleUrls: ['./mui-checkbox.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MuiCheckboxComponent),
    multi: true
  }],


})
export class MuiCheckboxComponent implements ControlValueAccessor {
  // Bindable properties
  @Input() text: string = '';
  @Input() disabled = false;
  @Input() indeterminate = false;
  @Input() anyvalue: string = '';
  // @HostBinding('class') get checkIfIndeterminate() {
  //  if (this.indeterminate) {
  //    return 'mui-checkbox-indeterminate';
  //  } else { return 'mui-checkbox'; }
  // }


  // Internal properties
  @Input() checked = false;
  onChange = ((_: any) => { });

  onBlur = ((_: any) => { });

  writeValue(obj: boolean): void {
    this.checked = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onBlur = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onChanged($event: any): void {
    this.checked = $event && $event.target && $event.target.checked;
    this.onChange(this.checked);
  }

  onBlured(): void {
    // this.onBlur(this.checked);
  }

}
