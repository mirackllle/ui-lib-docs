import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'mui-option',
  templateUrl: './mui-option.component.html',
  styleUrls: ['./mui-option.component.css']
})
export class MuiOptionComponent implements OnInit {

  @ViewChild('viewValue') viewValue!: ElementRef;
  @Input() value = '';
  public onOptionChosen: Subject<any> = new Subject<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onClick(): void {
    this.onOptionChosen.next({value: this.value, viewValue: this.viewValue.nativeElement.innerText});
  }

}
