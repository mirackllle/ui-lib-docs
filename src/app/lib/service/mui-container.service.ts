import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MuiContainerService {

  private containerElement: HTMLDivElement| null = null;

  constructor() { }

  private createContainer(): void {
    const containerClass = 'mui-overlay-container';
    const container = document.createElement('div');
    container.classList.add(containerClass);
    container.id = 'mui-overlay-container';
    container.style.position = 'fixed';
    container.style.zIndex = '1000';
    container.style.pointerEvents = 'none';
    container.style.top = '0';
    container.style.left = '0';
    container.style.height = '100%';
    container.style.width = '100%';
    container.style.display = 'flex';
    container.style.justifyContent = 'center';
    document.body.appendChild(container);
    this.containerElement = container;
  }

  public getContainer(): any {
    if (!this.containerElement) {
      this.createContainer();
    }
    return this.containerElement;
  }
}
