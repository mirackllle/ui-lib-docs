import { TestBed } from '@angular/core/testing';

import { MuiSnackBarService } from './mui-snack-bar.service';

describe('muiSnackBarService', () => {
  let service: muiSnackBarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(muiSnackBarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
