import {
  ApplicationRef,
  ComponentFactoryResolver,
  EmbeddedViewRef,
  Injectable,
  Injector,
  Renderer2,
  RendererFactory2
} from '@angular/core';
import {MuiContainerService} from './mui-container.service';
import {MuiSnackBarComponent} from '../mui-snack-bar/mui-snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class MuiSnackBarService {

  renderer: Renderer2;
  domElem: any;
  componentRef: any;

  constructor(private container: MuiContainerService, private factoryResolver: ComponentFactoryResolver,
              rendererFactory: RendererFactory2, private injector: Injector, private appRef: ApplicationRef) {
    this.renderer = rendererFactory.createRenderer(null, null);
    this.deleteSnackBar = this.deleteSnackBar.bind(this);
  }

  public openSuccessSnackBar(massage: string, ms?: number, closeButton?: boolean): void {
    this.domElem = this.addSnackBar(massage, 'success', closeButton);
    this.deleteSnackBar(ms);
  }

  public openErrorSnackBar(massage: string, ms?: number, closeButton?: boolean): void {
    this.domElem = this.addSnackBar(massage, 'error', closeButton);
    this.deleteSnackBar(ms);
  }

  public openInfoSnackBar(massage: string, ms?: number, closeButton?: boolean): void {
    this.domElem = this.addSnackBar(massage, 'info', closeButton);
    this.deleteSnackBar(ms);
  }

  private addSnackBar(massage: string, type?: string, closeButton?: boolean): HTMLElement {
    if (this.componentRef) { this.renderer.removeChild(this.container.getContainer(), this.domElem); }
    const factory = this.factoryResolver.resolveComponentFactory(MuiSnackBarComponent);
    this.componentRef = factory.create(this.injector);
    this.componentRef.instance.closeButton = closeButton;
    this.componentRef.instance.massage = massage;
    this.componentRef.instance.type = type;
    this.componentRef.instance.setCloseTrigger(this.deleteSnackBar);
    this.appRef.attachView(this.componentRef.hostView);
    const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    this.renderer.appendChild(this.container.getContainer(), domElem);
    return domElem;
  }

  public deleteSnackBar(ms?: number): void {
    if (ms) {
      setTimeout(() => {
        this.renderer.removeChild(this.container.getContainer(), this.domElem);
      }, ms);
    }
  }
}
