import { TestBed } from '@angular/core/testing';

import { MuiContainerService } from './mui-container.service';

describe('muiContainerService', () => {
  let service: muiContainerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(muiContainerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
