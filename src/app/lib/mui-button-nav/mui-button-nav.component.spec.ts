import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonNavComponent } from './mui-button-nav.component';

describe('muiButtonStraightComponent', () => {
  let component: MuiButtonNavComponent;
  let fixture: ComponentFixture<MuiButtonNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuiButtonNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
