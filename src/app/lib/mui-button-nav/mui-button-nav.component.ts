import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';


@Component({
  selector: 'mui-button-nav',
  templateUrl: './mui-button-nav.component.html',
  styleUrls: ['./mui-button-nav.component.css']
})
export class MuiButtonNavComponent implements OnInit{


  @Output() onClicked = new EventEmitter<any>();
  @Input() disabled = false;
  @Input() width = 'auto';
  @Input() type = 'button';

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    if (!this.disabled) {
      this.onClicked.emit();
    }
  }

}
