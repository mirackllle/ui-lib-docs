import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'mui-button-sec-nav',
  templateUrl: './mui-button-sec-nav.component.html',
  styleUrls: ['./mui-button-sec-nav.component.css']
})
export class MuiButtonSecNavComponent implements OnInit {

  @Output() onClicked = new EventEmitter<any>();
  @Input() disabled = false;
  @Input() width = 'auto';
  @Input() type = 'button';

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    if (!this.disabled) {
      this.onClicked.emit();
    }
  }
}
