import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonSecNavComponent } from './mui-button-sec-nav.component';

describe('muiButtonSecNavComponent', () => {
  let component: MuiButtonSecNavComponent;
  let fixture: ComponentFixture<MuiButtonSecNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiButtonSecNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonSecNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
