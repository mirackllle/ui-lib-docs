import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonComponent } from './mui-button.component';

describe('muiButtonComponent', () => {
  let component: MuiButtonComponent;
  let fixture: ComponentFixture<MuiButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
