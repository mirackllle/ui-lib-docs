import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'mui-button',
  templateUrl: './mui-button.component.html',
  styleUrls: ['./mui-button.component.css']
})
export class MuiButtonComponent implements OnInit {

  @Output() onClicked = new EventEmitter<any>();
  @Input() disabled = false;
  @Input() width = 'auto';
  @Input() type = 'button';

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    if (!this.disabled) {
      this.onClicked.emit();
    }
  }

}
