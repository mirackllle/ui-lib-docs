import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiTabSwitcherComponent } from './mui-tab-switcher.component';

describe('muiTabSwitcherComponent', () => {
  let component: MuiTabSwitcherComponent;
  let fixture: ComponentFixture<MuiTabSwitcherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiTabSwitcherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiTabSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
