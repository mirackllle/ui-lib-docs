import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ContentChildren,
  ElementRef,
  QueryList,
  AfterViewInit,
  ViewChildren, Renderer2
} from '@angular/core';
import {MuiLabelComponent} from '../mui-label/mui-label.component';

@Component({
  selector: 'mui-tab-switcher',
  templateUrl: './mui-tab-switcher.component.html',
  styleUrls: ['./mui-tab-switcher.component.css']
})
export class MuiTabSwitcherComponent implements OnInit, AfterViewInit {

  @Input() activeTab = 0;
  @Output() onTabChanged = new EventEmitter();
  @ContentChildren(MuiLabelComponent, {read: ElementRef}) labelElements!: QueryList<ElementRef>;
  @ViewChildren('labelContainer', { read: ElementRef })labelContainers!: QueryList<ElementRef>;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.labelElements.forEach((elem, index) => {
      this.renderer.appendChild(this.labelContainers.toArray()[index].nativeElement, elem.nativeElement);
    });
  }

  public changeTab(activeTab: any): void {
    if (this.activeTab !== activeTab) {
      this.activeTab = activeTab;
      this.onTabChanged.emit(activeTab);
    }
  }

}
