import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MuiButtonComponent } from './mui-button/mui-button.component';
import { MuiButtonSecComponent } from './mui-button-sec/mui-button-sec.component';
import { MuiNotifyComponent } from './mui-notify/mui-notify.component';
import { MuiIconsComponent} from './mui-icons/mui-icons.component';
import {MuiLabelComponent} from "./mui-label/mui-label.component";
import {MuiOptionComponent} from "./mui-option/mui-option.component";
import {MuiSelectComponent} from "./mui-select/mui-select.component";
import {MuiCheckboxComponent} from "./mui-checkbox/mui-checkbox.component"
import {MuiPaginatorComponent} from "./mui-paginator/mui-paginator.component";
import {MuiInputComponent} from "./mui-input/mui-input.component";
import {MuiButtonSecNavComponent} from "./mui-button-sec-nav/mui-button-sec-nav.component";
import {MuiButtonNavComponent} from "./mui-button-nav/mui-button-nav.component";
import {MuiSnackBarComponent} from "./mui-snack-bar/mui-snack-bar.component";
import {MuiSnackBarService} from "./service/mui-snack-bar.service";
import {MuiTabSwitcherComponent} from "./mui-tab-switcher/mui-tab-switcher.component";
import { PermissionDirective } from './mui-directives/mui-permission-directive/permission-directive';
import { MuiInputDirective } from './mui-input/mui-input.directive';
import { RangepickerComponent } from './mui-rangepicker/rangepicker.component';
import { CalendarBodyComponent } from './mui-rangepicker/components/calendar-body/calendar-body.component';
import { CalendarHeaderComponent } from './mui-rangepicker/components/calendar-header/calendar-header.component';
import { DaysViewComponent } from './mui-rangepicker/components/days-view/days-view.component';
import { MonthsViewComponent } from './mui-rangepicker/components/months-view/months-view.component';
import { YearViewComponent } from './mui-rangepicker/components/year-view/year-view.component';
import { MuiEndDateDirective } from './mui-rangepicker/directives/end-date/end-date.directive';
import { MuiStartDateDirective } from './mui-rangepicker/directives/start-date/start-date.directive';

@NgModule({
  declarations: [
    MuiButtonComponent,
    MuiButtonSecComponent,
    MuiButtonNavComponent,
    MuiButtonSecNavComponent,
    MuiNotifyComponent,
    MuiIconsComponent,
    MuiLabelComponent, 
    MuiOptionComponent,
    MuiSelectComponent,
    MuiCheckboxComponent,
    MuiPaginatorComponent,
    MuiInputComponent,
    PermissionDirective,
    MuiSnackBarComponent,
    MuiInputDirective,
    MuiTabSwitcherComponent,
    RangepickerComponent,
    CalendarBodyComponent,
    CalendarHeaderComponent,
    DaysViewComponent,
    MonthsViewComponent,
    YearViewComponent,
    MuiStartDateDirective,
    MuiEndDateDirective
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    MuiButtonComponent,
    MuiButtonSecComponent,
    MuiButtonNavComponent,
    MuiButtonSecNavComponent,
    MuiNotifyComponent,
    MuiIconsComponent,
    MuiLabelComponent,
    MuiOptionComponent,
    MuiSelectComponent,
    MuiCheckboxComponent,
    MuiPaginatorComponent,
    MuiInputComponent,
    PermissionDirective,
    MuiInputDirective,
    MuiTabSwitcherComponent,
    RangepickerComponent,
    MuiStartDateDirective,
    MuiEndDateDirective
  ],
  providers: [
    MuiSnackBarService
  ]
})
export class UiLibModule { }
