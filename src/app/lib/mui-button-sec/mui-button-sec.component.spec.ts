import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonSecComponent } from './mui-button-sec.component';

describe('muiButtonSecComponent', () => {
  let component: MuiButtonSecComponent;
  let fixture: ComponentFixture<MuiButtonSecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiButtonSecComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
