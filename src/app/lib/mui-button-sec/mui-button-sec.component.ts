import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'mui-button-sec',
  templateUrl: './mui-button-sec.component.html',
  styleUrls: ['./mui-button-sec.component.css']
})
export class MuiButtonSecComponent implements OnInit {

  @Output() onClicked = new EventEmitter<any>();
  @Input() disabled = false;
  @Input() width = 'auto';
  @Input() type = 'button';

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    if (!this.disabled) {
      this.onClicked.emit();
    }
  }
}
