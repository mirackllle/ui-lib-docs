import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'mui-label',
  templateUrl: './mui-label.component.html',
  styleUrls: ['./mui-label.component.css']
})
export class MuiLabelComponent implements OnInit {

  @ViewChild('labelDiv') labelDiv!: ElementRef;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
  }

  addClass(className: string): void {
    this.renderer.addClass(this.labelDiv.nativeElement, className);
  }

}
