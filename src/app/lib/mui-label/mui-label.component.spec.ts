import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiLabelComponent } from './mui-label.component';

describe('muiLabelComponent', () => {
  let component: MuiLabelComponent;
  let fixture: ComponentFixture<MuiLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
