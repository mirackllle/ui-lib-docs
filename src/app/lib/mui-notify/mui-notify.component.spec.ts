import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiNotifyComponent } from './mui-notify.component';

describe('muiNotifyComponent', () => {
  let component: MuiNotifyComponent;
  let fixture: ComponentFixture<MuiNotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuiNotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiNotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
