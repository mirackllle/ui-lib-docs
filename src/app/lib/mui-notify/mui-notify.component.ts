import {AfterViewInit, ChangeDetectorRef, Component, Input} from '@angular/core';

@Component({
  selector: 'mui-notify',
  templateUrl: './mui-notify.component.html',
  styleUrls: ['./mui-notify.component.css']
})
export class MuiNotifyComponent implements AfterViewInit {
  @Input() visible = true;
  @Input() icon = 'warning';
  @Input() background: string | undefined;
  newBcgColor = {replace: false, value: ''};
  constructor(private cd: ChangeDetectorRef) { }

  ngAfterViewInit() {
    if (this.background) {
      this.changeBcgColor(this.background);
    }
    this.cd.detectChanges();
  }

  changeBcgColor(elem: any): void {
    if (elem.startsWith('--c')) {
      this.newBcgColor = {replace: true, value: 'var(' + elem + ', #EB6826)'};
    } else if (elem.match(/^#[a-fA-F0-9]{3}$/g) || elem.match(/^#[a-fA-F0-9]{6}$/g)) {
      this.newBcgColor = {replace: true, value: elem};
    } else if (elem.startsWith('#')) {console.error('Background value must contain 3 or 6 symbols after #. Example: #FFF or #FFFFFF')}
  }

}
