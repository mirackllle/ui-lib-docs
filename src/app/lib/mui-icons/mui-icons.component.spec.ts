import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiIconsComponent } from './mui-icons.component';

describe('muiIconsComponent', () => {
  let component: MuiIconsComponent;
  let fixture: ComponentFixture<MuiIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuiIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
