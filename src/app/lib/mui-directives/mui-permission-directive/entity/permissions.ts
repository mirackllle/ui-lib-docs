import {AuthObjects} from "./auth-objects";

export class ListOfPermissions {
public permissions: AuthObjects[];

  constructor(permissions: AuthObjects[]) {
    this.permissions = permissions;
  }
}
