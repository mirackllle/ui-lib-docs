export class AuthObjects {
  public actionCode: number;
  public code: number;
  public url: string | undefined;

  constructor(actionCode: number, code: number, url?: string) {
    this.actionCode = actionCode;
    this.code = code;
    this.url = url;
  }
}
