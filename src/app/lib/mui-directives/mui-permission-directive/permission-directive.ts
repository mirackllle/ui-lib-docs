import {Directive, HostBinding, Input, OnInit} from '@angular/core';
import {ShortRoleAuthority} from './entity/short-role-authority';

@Directive({
  selector: '[muiPerm]',
})
export class PermissionDirective implements OnInit {

  @Input() public requiredPermissions: any = false;
  @Input() public listOfAuthorities: any;

  private disable = true;

  constructor() {
  }

  public ngOnInit(): void {
    this.disable = !this.checkPermissions();
  }

  @HostBinding('hidden') get getDisabled(): boolean {
    return this.disable;
  }

  public checkPermissions(): boolean {
    if (!this.requiredPermissions) {
      return false;
    }
    const existingPermissions: any[] = this.listOfAuthorities;
    if (!existingPermissions) {
      return false;
    }
    if (!Array.isArray(this.requiredPermissions)) {
      this.requiredPermissions = [this.requiredPermissions];
    }
    return existingPermissions.some(
      (existPermission) =>
        this.requiredPermissions.some(
          (requiredPermission: ShortRoleAuthority) => {
            if (!Array.isArray(requiredPermission.actionCode)) {
              requiredPermission.actionCode = [requiredPermission.actionCode];
            }
            return (requiredPermission.code === existPermission.code &&
              requiredPermission.actionCode.some((actionCode : any) => actionCode === existPermission.actionCode));
          },
        ));
  }
}
