import {
  AfterViewInit, ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  ElementRef, EventEmitter,
  forwardRef, Input,
  Output,
  QueryList,
  Renderer2,
  ViewChild
} from '@angular/core';
import {MuiLabelComponent} from '../mui-label/mui-label.component';
import {MuiOptionComponent} from '../mui-option/mui-option.component';
import {ControlValueAccessor, FormControlName, NG_VALUE_ACCESSOR, NgControl} from '@angular/forms';

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MuiLabelComponent),
  multi: true
};

@Component({
  selector: 'mui-select',
  templateUrl: './mui-select.component.html',
  styleUrls: ['./mui-select.component.css'],
  // providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class MuiSelectComponent implements AfterViewInit, ControlValueAccessor {

  @Input() isDisabled = false;
  @Input() width = '300px';
  @Input() autocomplete = false;
  @Output() onSelectionChanged = new EventEmitter<string>();
  @ContentChild(MuiLabelComponent, {read: ElementRef}) label!: ElementRef;
  @ContentChildren(MuiOptionComponent, {read: ElementRef}) optionsElement!: QueryList<ElementRef>;
  @ContentChildren(MuiOptionComponent, {read: MuiOptionComponent}) optionsComponent!: QueryList<MuiOptionComponent>;
  @ViewChild('labelContainer') labelContainer!: ElementRef;
  @ViewChild('optionsContainer') optionsContainer!: ElementRef;
  @ViewChild('input') input!: ElementRef;
  value = '';
  viewValue = '';
  isOpen = false;
  setValue: any;
  registerTouch: any;
  isFirstTouch = true;
  control: FormControlName;

  constructor(private renderer: Renderer2, private cd: ChangeDetectorRef, public ngControl: NgControl) {
    ngControl.valueAccessor = this;
    this.control = ngControl as FormControlName;
    this.onOptionChanged = this.onOptionChanged.bind(this);
  }

  ngAfterViewInit(): void {
    this.writeValue(this.value);
    this.renderer.appendChild(this.labelContainer.nativeElement, this.label.nativeElement);
    this.optionsElement.forEach(elem => {
      this.renderer.appendChild(this.optionsContainer.nativeElement, elem.nativeElement);
    });
    this.optionsComponent.forEach(elem => {
      elem.onOptionChosen.subscribe(this.onOptionChanged);
    });

    this.optionsElement.changes.subscribe((data: QueryList<ElementRef>) => {
      data.forEach(elem => {
        this.renderer.appendChild(this.optionsContainer.nativeElement, elem.nativeElement);
      });
    });

    this.optionsComponent.changes.subscribe((data: QueryList<MuiOptionComponent>) => {
      data.forEach(elem => {
        elem.onOptionChosen.subscribe(this.onOptionChanged);
      });
    });
    this.cd.detectChanges();
  }

  switchStatus(): void {
    if (!this.isDisabled) {
      this.isOpen = !this.isOpen;

    }
    if (this.isFirstTouch && this.registerTouch) {
      this.registerTouch();
      this.isFirstTouch = false;
    }
  }

  setStatus(status: any): void {
    if (this.isDisabled) {
      return;
    }
    this.isOpen = status;
    if (this.autocomplete) {
      setTimeout(() => this.input.nativeElement.focus());
    }
  }

  onOptionChanged(event: any): void {
    this.onSelectionChanged.emit(event.value);
    this.value = event.value;
    this.viewValue = event.viewValue;
    this.isOpen = false;
    this.restoreOptions();
    if (this.setValue) { this.setValue(this.value); }
  }

  onFocusOut(): void {
    this.isOpen = false;
    this.restoreOptions();
  }

  restoreOptions(): void {
    for (const child of this.optionsContainer.nativeElement.children) {
      this.renderer.removeChild(this.optionsContainer.nativeElement, child);
    }
    this.optionsElement.forEach(elem => {
      this.renderer.appendChild(this.optionsContainer.nativeElement, elem.nativeElement);
    });
    this.viewValue = '';
    this.cd.detectChanges();
    this.viewValue = this.getViewValueByValue(this.value).viewValue;
  }

  registerOnChange(fn: any): void {
    this.setValue = fn;
  }

  registerOnTouched(fn: any): void {
    this.registerTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this.isOpen = false;
  }

  writeValue(obj: any): void {
    this.value = obj;
    const value = this.getViewValueByValue(obj);
    this.value = value.value;
    this.viewValue = value.viewValue;
  }

  getViewValueByValue(value: any): any {
    if (!this.optionsComponent) {
      return {value, viewValue: ''};
    }
    const component = this.optionsComponent.find(elem => elem.value === value);
    if (component) { return {value: component.value, viewValue: component.viewValue.nativeElement.innerText}; }
    else { return {value: '', viewValue: ''}; }
  }

  filterOptions(): void {
    if (!this.optionsComponent) {
      return;
    }
    for (const child of this.optionsContainer.nativeElement.children) {
      this.renderer.removeChild(this.optionsContainer.nativeElement, child);
    }
    this.optionsElement.forEach(elem => {
      if (elem.nativeElement.firstChild.innerText.toLowerCase().indexOf(this.input.nativeElement.value.toLowerCase()) !== -1){
        this.renderer.appendChild(this.optionsContainer.nativeElement, elem.nativeElement);
      }
    });
    this.cd.detectChanges();
  }

}
