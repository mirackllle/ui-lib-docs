import {MuiPaginatorComponent} from './mui-paginator.component';
import { FormBuilder } from '@angular/forms';

describe('Form product details component', () => {
  it('should be create', function () {
    let component = setup().build();
    expect(component).toBeDefined();
  });

  it('onClick should set page', function () {
    let component = setup().build();
    component.pageCount = 3;
    component.onClick(0);
    expect(component.pages).toEqual([0, 1, 2]);
  });

  it('onClick should set page', function () {
    let component = setup().build();
    component.pageCount = 10;
    component.onClick(4);
    expect(component.pages).toEqual([2, 3, 4, 5, 6]);
  });

  it('onClick should set page', function () {
    let component = setup().build();
    component.pageCount = 10;
    component.onClick(9);
    expect(component.pages).toEqual([5, 6, 7, 8, 9]);
  });
});

function setup() {
  const builder = {
    default() {
      return builder;
    },
    build() {
      const component = new MuiPaginatorComponent();
      // directive.prop = {selected: false};
      return component;
    }
  };

  return builder;
}
