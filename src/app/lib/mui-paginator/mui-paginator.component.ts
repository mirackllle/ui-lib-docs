import {Component} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'mui-paginator',
  templateUrl: './mui-paginator.component.html',
  styleUrls: ['./mui-paginator.component.css']
})
export class MuiPaginatorComponent {

  count = 1;
  pages = [0];
  currentPage = 0;
  onPageChange: BehaviorSubject<number> = new BehaviorSubject(0);
  paginatorSize = 5;

  constructor() {
  }

  set pageCount(value: number) {
    this.paginatorSize = this.paginatorSize > value ? value : this.paginatorSize;
    this.pages = Array(this.paginatorSize).fill(1).map((x, i) => i);
    this.count = value;
  };

  onClick(page: any) {
    this.currentPage = page;
    this.onPageChange.next(this.currentPage);
    this.pages = Array(this.paginatorSize).fill(1).map((x, i) => {
      if (this.currentPage - Math.floor(this.paginatorSize / 2) < 1) {
        return i
      }
      if (this.count - this.currentPage < Math.ceil(this.paginatorSize / 2)) {
        return i + this.count - this.paginatorSize
      }
      return i + this.currentPage - Math.floor(this.paginatorSize / 2);
    });
  }

}
