import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiSnackBarComponent } from './mui-snack-bar.component';

describe('muiSnackBarComponent', () => {
  let component: MuiSnackBarComponent;
  let fixture: ComponentFixture<MuiSnackBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiSnackBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
