import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'mui-snack-bar',
  templateUrl: './mui-snack-bar.component.html',
  styleUrls: ['./mui-snack-bar.component.css'],
  animations: [
    trigger('openClose', [
      state('open', style({
        opacity: 1
      })),
      state('closed', style({
        opacity: 0
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ]),
    ]),
  ]
})
export class MuiSnackBarComponent implements OnInit {

  public closeButton: boolean = false;
  public massage = '';
  public type = 'info';
  public isOpen = false;
  private closeFn: any;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.isOpen = true;
    }, 4);
  }

  closeSnackBar(): void {
    this.isOpen = false;
    setTimeout(() => {
      this.closeFn(1);
    }, 300);
  }

  setCloseTrigger(fn: any): void {
    this.closeFn = fn;
  }

}
