import { AfterViewInit, Component, ContentChild, ElementRef, Input, OnChanges, Renderer2,
         SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CalendarBodyService } from './components/calendar-body/calendar-body.service';
import { CalendarView, Lang } from './interfaces/enums';
import { RangepickerService } from './rangepicker.service';
import { MuiLabelComponent } from './../mui-label/mui-label.component';

@Component({
  selector: 'mui-rangepicker',
  templateUrl: './rangepicker.component.html',
  styleUrls: ['./rangepicker.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [CalendarBodyService, RangepickerService]
})
export class RangepickerComponent implements OnChanges, AfterViewInit {
  @Input() formGroup!: FormGroup;
  @Input() width: number = 290;
  @Input() lang: any = Lang.UA;
  @ContentChild(MuiLabelComponent, {read: ElementRef}) labelEl!: ElementRef;
  @ViewChild('labelContainer') labelContainer!: ElementRef;
  @ViewChild('dateFrom') dateFromEl!: ElementRef;
  @ViewChild('dateTo') dateToEl!: ElementRef;
  
  id: string = 'rangepicker';
  CALENDAR_VIEW = CalendarView;

  constructor(public calendarBodyService: CalendarBodyService,
              private _rangepickerService: RangepickerService,
              private _renderer: Renderer2) {
    this.calendarBodyService.id = this.id;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes?.formGroup?.currentValue) this._rangepickerService.formGroup = this.formGroup;
    if (changes?.lang.currentValue) this._rangepickerService.setLang(this.lang);
    if (changes?.width?.currentValue) this.calendarBodyService.width = this.width;
  }

  ngAfterViewInit() {
    this._renderer.appendChild(this.labelContainer.nativeElement, this.labelEl.nativeElement);
    this._rangepickerService.dateFromControlName = (this.dateFromEl.nativeElement as HTMLElement).children[0].getAttribute('formcontrolname') as string;
    this._rangepickerService.dateToControlName = (this.dateToEl.nativeElement as HTMLElement).children[0].getAttribute('formcontrolname') as string;

  }

  get isError(): boolean {
    return !!(!this._rangepickerService.isToggle && this.formGroup.controls[this._rangepickerService.dateFromControlName]?.invalid || this.formGroup.controls[this._rangepickerService.dateToControlName]?.invalid);
  }
  
  get isDisabled(): boolean {
    return this.formGroup.controls[this._rangepickerService.dateFromControlName]?.disabled || this.formGroup.controls[this._rangepickerService.dateToControlName]?.disabled;
  }

  get isFocused(): boolean {
    return this.formGroup.controls[this._rangepickerService.dateFromControlName]?.touched || this.formGroup.controls[this._rangepickerService.dateToControlName]?.touched || this.calendarBodyService.isToggle;
  }

}

