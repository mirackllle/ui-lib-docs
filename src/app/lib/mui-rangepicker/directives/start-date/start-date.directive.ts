import { Directive, ElementRef, Renderer2 } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { RangeCell } from "../../interfaces/interfaces";
import { RangepickerService } from '../../rangepicker.service';

@Directive({
  selector: 'input[MuiStartDate]',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MuiStartDateDirective, multi: true }
  ]
})

export class MuiStartDateDirective implements ControlValueAccessor {
  onChange!: (value: any) => void;
  onTouched!: () => void;
  constructor(private _elementRef: ElementRef<HTMLInputElement>,
    private _rangepickerService: RangepickerService,
    private _renderer: Renderer2) {
    this._setListeners();
  }

  writeValue(val: Date): void {
    this._elementRef.nativeElement.value = this._rangepickerService.format(val);
    const date = val ? new Date(val.getFullYear(), val.getMonth(), val.getDate(), 0) : new Date();
    this._rangepickerService.range.dateFrom = new RangeCell(date.getDate(), date.getTime());
    this._rangepickerService.setDate(date);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._renderer.setProperty(this._elementRef.nativeElement, 'disabled', isDisabled);
  }

  private _setListeners(): void {
    this._elementRef.nativeElement.onblur = () => {
      this._rangepickerService.formGroup.controls[this._rangepickerService.dateFromControlName].markAsUntouched();
      this._rangepickerService.formatDateInput(this._elementRef.nativeElement.value, this._rangepickerService.dateFromControlName);
    }
    this._elementRef.nativeElement.onfocus = () => this._rangepickerService.formGroup.controls[this._rangepickerService.dateFromControlName].markAsTouched();
    this._elementRef.nativeElement.oninput = () => {
      this._rangepickerService.formatString(this._elementRef);
      this._rangepickerService.formatDateInput(this._elementRef.nativeElement.value, this._rangepickerService.dateFromControlName);
    }
  }

}