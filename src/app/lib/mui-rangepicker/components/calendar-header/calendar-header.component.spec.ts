import { RangepickerService } from '../../rangepicker.service';

import { CalendarHeaderComponent } from './calendar-header.component';

describe('CalendarHeaderComponent', () => {
  let component: CalendarHeaderComponent;
  let service = {};

  beforeEach(() => {
    component = new CalendarHeaderComponent(service as unknown as RangepickerService)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
