import { Component } from '@angular/core';
import { Range } from '../../interfaces/interfaces';
import { RangepickerService } from '../../rangepicker.service';
import { CalendarView, DateFormat, Period } from '../../interfaces/enums';
@Component({
  selector: 'app-calendar-header',
  templateUrl: './calendar-header.component.html',
  styleUrls: ['./calendar-header.component.css']
})
export class CalendarHeaderComponent {
  private _currentDate: Date;
  private _period!: Date;
  constructor(private _rangepickerService: RangepickerService) {
                this._currentDate = new Date();
  }

  onChangePeriod(val: Period): void {
    if (this._rangepickerService.view === CalendarView.DAY) this._period = new Date(this._currentDate.getFullYear(), this._currentDate.getMonth() + val);
    if (this._rangepickerService.view === CalendarView.YEAR) this._period = new Date(this._currentDate.getFullYear() + val * 24, 0);
    if (this._rangepickerService.view === CalendarView.MONTH) this._period = new Date(this._currentDate.getFullYear() + val, 0);
    this._rangepickerService.setDate(this._period);
  }

  onChangeView(): void {
    this._rangepickerService.view = this._rangepickerService.view === CalendarView.DAY ? CalendarView.YEAR : CalendarView.DAY;
    this._rangepickerService.range = new Range();
    this._rangepickerService.setDate(this._currentDate);
  }

  get periodButtonText(): string {
    let result = '';
    if (this._rangepickerService.view === CalendarView.DAY) result = this._getDayViewLabel();
    if (this._rangepickerService.view === CalendarView.YEAR) result = this._getYearViewLabel();
    if (this._rangepickerService.view === CalendarView.MONTH) result = this._getMonthViewLabel();
    return result;
  }

  private _getDayViewLabel(): string {
    const i = this._rangepickerService.rows.length - 1;
    const j = this._rangepickerService.rows[i].length - 1;
    this._currentDate = new Date(this._rangepickerService.rows[i][j].compareValue);
    const month = this._rangepickerService.getMonthsDays(DateFormat.LONG)[this._currentDate.getMonth()];
    const year = this._currentDate.getFullYear();
    return `${month} ${year}`;
  }

  private _getYearViewLabel(): string {
    const yearStart = this._rangepickerService.rows[0][0].value;
    this._currentDate = new Date(yearStart + 5, 0);
    const i = this._rangepickerService.rows.length - 1;
    const j = this._rangepickerService.rows[i].length - 1;
    const yearEnd = this._rangepickerService.rows[i][j].value;
    return `${yearStart} - ${yearEnd}`;
  }

  private _getMonthViewLabel(): string {
    const date = this._rangepickerService.rows[0][0].compareValue;
    this._currentDate = new Date(date);
    return this._currentDate.getFullYear().toString();
  }

}
