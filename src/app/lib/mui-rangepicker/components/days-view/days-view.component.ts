import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Range, RangeCell } from '../../interfaces/interfaces';
import { RangepickerService } from '../../rangepicker.service';
import { CalendarBodyService } from '../calendar-body/calendar-body.service';

@Component({
  selector: 'mui-rangepicker-days-view',
  templateUrl: './days-view.component.html',
  styleUrls: ['./days-view.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DaysViewComponent implements OnInit {
  _cellWidth!: string;
  _cellPadding!: string;
  _previewEnd!: number;
  private _today: Date = new Date();
  constructor(public rangepickerService: RangepickerService,
              private _calendarBodyService: CalendarBodyService) { }

  ngOnInit(): void {
    const numCols = 7;
    const cellAspectRatio = 1;
    this._cellWidth = `${100 / numCols}%`;
    this._cellPadding = `${(50 * cellAspectRatio) / numCols}%`;
  }

  onCellClick(cell: RangeCell): void {
    if (this.rangepickerService.range.dateFrom?.compareValue && this.rangepickerService.range.dateTo?.compareValue) {
      this.rangepickerService.range = new Range();
    }
    if (!this.rangepickerService.range.dateFrom?.compareValue || this.rangepickerService.range.dateFrom.compareValue > cell.compareValue) {
      this.rangepickerService.range.dateFrom = new RangeCell(cell.value, cell.compareValue);
      this.rangepickerService.formGroup.controls[this.rangepickerService.dateFromControlName].setValue(new Date(cell.compareValue));
      return;
    }
    if (!this.rangepickerService.range.dateTo?.compareValue) {
      this.rangepickerService.range.dateTo = new RangeCell(cell.value, cell.compareValue);
      this.rangepickerService.formGroup.controls[this.rangepickerService.dateFromControlName].markAsUntouched();
      this.rangepickerService.formGroup.controls[this.rangepickerService.dateToControlName].markAsUntouched();
      this.rangepickerService.formGroup.controls[this.rangepickerService.dateToControlName].setValue(new Date(cell.compareValue));
      this._calendarBodyService.onToggle();
      return;
    }
  }

  _isRangeStart(val: number): boolean {
    return this.rangepickerService.range?.dateFrom?.compareValue === val
      || this._isToday(val) && !this.rangepickerService.range.dateFrom;
  }

  _isRangeEnd(val: number): boolean {
    return this.rangepickerService.range.dateTo?.compareValue === val;
  }

  _isInRange(val: number): boolean {
    return this.rangepickerService.range.dateFrom?.compareValue <= val && this.rangepickerService.range.dateTo?.compareValue >= val;
  }

  _isToday(val: number): boolean {
    return new Date(this._today.getFullYear(), this._today.getMonth(), this._today.getDate()).getTime() === val;
  }

  _isInPreview(val: number): boolean {
    if (this._isRange()) return false;
    return val >= this.rangepickerService.range.dateFrom?.compareValue
      && val <= this._previewEnd
      && this._previewEnd > this.rangepickerService.range.dateFrom?.compareValue;
  }

  _isPreviewStart(val: number): boolean {
    if (this._isRange()) return false;
    return this.rangepickerService.range.dateFrom?.compareValue === val 
      && this._previewEnd !== this.rangepickerService.range.dateFrom.compareValue
      && this._previewEnd > this.rangepickerService.range.dateFrom?.compareValue;
  }

  _isPreviewEnd(val: number): boolean {
    if (this._isRange()) return false;
    return this._previewEnd === val
      && this._previewEnd > this.rangepickerService.range.dateFrom?.compareValue;
  }

  private _isRange(): boolean {
    return !!(this.rangepickerService.range.dateFrom?.compareValue && this.rangepickerService.range.dateTo?.compareValue);
  }

}
