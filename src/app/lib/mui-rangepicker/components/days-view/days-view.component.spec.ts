import { RangepickerService } from '../../rangepicker.service';

import { DaysViewComponent } from './days-view.component';

describe('DaysViewComponent', () => {
  let component: DaysViewComponent;
  let service = {};

  beforeEach(() => {
    component = new DaysViewComponent(service as unknown as RangepickerService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
