import { Component, OnInit } from '@angular/core';
import { CalendarView } from '../../interfaces/enums';
import { RangepickerService } from '../../rangepicker.service';

@Component({
  selector: 'app-calendar-body',
  templateUrl: './calendar-body.component.html',
  styleUrls: ['./calendar-body.component.css']
})
export class CalendarBodyComponent implements OnInit {
  width!: number;
  CALENDAR_VIEW = CalendarView;
  id!: string;
  private _rangepicker: any = document.getElementById('rangepicker');
  constructor(public rangepickerService: RangepickerService) {
    
  }

  ngOnInit(): void {
  }

  get top(): number {
    return this._rangepicker.getBoundingClientRect().top + this._rangepicker.offsetHeight;
  }

  get left(): number {
    return this._rangepicker.getBoundingClientRect().left;
  }

  get months(): string[] {
    return this.rangepickerService.getMonthsDays();
  }

  onYearViewClick(date: any): void {
    this.rangepickerService.view = CalendarView.MONTH;
    this.rangepickerService.setDate(date);
  }

  onMonthViewClick(date: any): void {
    this.rangepickerService.view = CalendarView.DAY;
    this.rangepickerService.setDate(date);
  }

}
