import { ApplicationRef, ComponentFactoryResolver, EmbeddedViewRef, Injectable, Injector, Renderer2, RendererFactory2 } from "@angular/core";
import { MuiContainerService } from "../../../service/mui-container.service";
import { CalendarView } from "../../interfaces/enums";
import { RangepickerService } from "../../rangepicker.service";
import { CalendarBodyComponent } from "./calendar-body.component";

@Injectable()
export class CalendarBodyService {
    isToggle!: boolean;
    width!: number;
    id!: string;
    private _componentRef: any;
    private _container!: HTMLElement;
    private _menuEl!: HTMLElement;
    private _renderer: Renderer2;
    
    constructor(private _muiContainerService: MuiContainerService,
        private _factoryResolver: ComponentFactoryResolver,
        private _rendererFactory: RendererFactory2,
        private _injector: Injector,
        private _appRef: ApplicationRef,
        private _rangepickerService: RangepickerService) {
        this._renderer = this._rendererFactory.createRenderer(null, null);
    }

    onToggle(): void {
        this.isToggle = !this.isToggle;
        this.isToggle ? this._attachView() : this._detachView();
    }

    private _attachView(): void {
        this._container = this._muiContainerService.getContainer();
        const factory = this._factoryResolver.resolveComponentFactory(CalendarBodyComponent);
        this._componentRef = factory.create(this._injector);
        this._componentRef.instance.width = this.width;
        this._componentRef.instance.id = this.id;
        this._appRef.attachView(this._componentRef.hostView);
        this._menuEl = (this._componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        this._componentRef.instance.domElem = this._menuEl;
        this._renderer.appendChild(this._container, this._menuEl);
    }
    
    private _detachView(): void {
        this._renderer.removeChild(this._container, this._menuEl);
        this._rangepickerService.view = CalendarView.DAY;
    }
}