import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { RangeCell } from '../../interfaces/interfaces';

@Component({
  selector: 'mui-rangepicker-year-view',
  templateUrl: './year-view.component.html',
  styleUrls: ['./year-view.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class YearViewComponent implements OnInit {
  @Input() date!: RangeCell | undefined;
  @Input() rows!: RangeCell[][] | undefined;
  @Output() cellClick = new EventEmitter<Date>();
  _cellWidth!: string;
  _cellPadding!: string;
  private _today: Date = new Date();
  constructor() { }

  ngOnInit(): void {
    const numCols = 7;
    const cellAspectRatio = 1;
    this._cellWidth = `${100 / numCols}%`;
    this._cellPadding = `${(50 * cellAspectRatio) / numCols}%`;
  }

  onCellClick(cell: RangeCell): void {
    this.cellClick.emit(new Date(cell.value, this._today.getMonth()));
  }

  _isToday(val: number): boolean {
    return new Date(this._today.getFullYear()).getTime() === val;
  }

  _isRangeStart(val: number): boolean {
    return this.date?.compareValue === val
      || this._isToday(val) && !this.date;
  }

}
