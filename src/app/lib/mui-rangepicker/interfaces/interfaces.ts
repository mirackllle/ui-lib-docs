export class Range {
    dateFrom: RangeCell;
    dateTo: RangeCell;

    constructor(dateFrom?: RangeCell, dateTo?: RangeCell) {
        this.dateFrom = dateFrom as RangeCell;
        this.dateTo = dateTo as RangeCell;
    }
}

export class RangeCell {
    value: number;
    compareValue: number;

    constructor(value: number, compareValue: number) {
        this.value = value;
        this.compareValue = compareValue;
    }
}

export interface RangeForm {
    dateFrom: string;
    dateTo: string;
}