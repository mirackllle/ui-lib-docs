export enum CalendarView {
    YEAR = 'year',
    MONTH = 'month',
    DAY = 'day'
}

export enum Period {
    NEXT = 1,
    PREV = -1
}

export enum Lang {
    UA = 'ua',
    EN = 'en'
}

export enum DateFormat {
    LONG = 'long',
    SHORT = 'short'
}