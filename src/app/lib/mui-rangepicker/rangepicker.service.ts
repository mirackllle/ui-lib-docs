import { ElementRef, Injectable, InjectionToken } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CalendarView, DateFormat, Lang } from './interfaces/enums';
import { RangeCell, Range } from './interfaces/interfaces';
import * as TRANSLATION from './dates.json';

export const MUI_RANGEPICKER = new InjectionToken<RangepickerService>('MUI_RANGEPICKER');

@Injectable()
export class RangepickerService {
    weeks!: number;
    startDay!: number;
    totalDays!: number;
    startYear!: number;
    endYear!: number;
    rows: RangeCell[][] = [];
    translation: any;
    view: CalendarView = CalendarView.DAY;
    range: Range = new Range();
    isToggle!: boolean;
    dateFromControlName!: string;
    dateToControlName!: string;
    private _formGroup!: FormGroup;
    private _date!: Date;
    private _lang!: Lang;

    constructor() {
    }

    getWeekDays(): string[] {
        const weekDays: string[] = Object.values(this.translation['weekDays']);
        const tails = weekDays.slice(0, this.getFirstDay());
        weekDays.splice(0, this.getFirstDay());
        return weekDays.concat(tails);
    }

    getMonthsDays(format: DateFormat = DateFormat.SHORT): string[] {
        return Object.values(this.translation['dateMonths'][format]);
    }

    setLang(lang: Lang): void {
        this._lang = lang;
        this.translation = TRANSLATION[this._lang];
    }

    setDate(date: Date): void {
        this.rows = [];
        if (this.view === CalendarView.DAY) this._setDayView(date);
        if (this.view === CalendarView.YEAR) this._setYearView(date);
        if (this.view === CalendarView.MONTH) this._setMonthsView(date);
    }

    getFirstDay(): number {
        return 1;
    }

    format(date: Date): string {
        let res = '';
        if (date) {
            const month = date.getMonth() < 9 ? `0${date.getMonth() + 1}` : (date.getMonth() + 1);
            const day = date.getDate().toString().length < 2 ? `0${date.getDate()}` : date.getDate();
            res = `${day}.${month}.${date.getFullYear()}`
        }
        return res;
    }

    parse(val: string): Date | null {
        const vals: string[] = val.split('.');
        const month = +vals[1] || null;
        const day = +vals[0] || null;
        const year = +vals[2] || null;
        const str = `${month}.${day}.${year}`;
        return Date.parse(str) ? new Date(str) : null;
    }

    formatDateInput(value: string, key: string): void{
        const date = this.parse(value);
        if (!date) this.formGroup.controls[key].setErrors({wrongFormat: true});
        if (date) this.formGroup.controls[key].setValue(date);
    }

    formatString(el: ElementRef): void {
        const nativeElement = el.nativeElement;
        const mask = nativeElement.getAttribute('data-mask');
        const pattern = nativeElement.getAttribute('data-format');
        const value = this._formatString(nativeElement.value, pattern);
        nativeElement.value = this._formatString(nativeElement.value, pattern, mask);
        if (nativeElement.selectionStart) {
            nativeElement.focus();
            nativeElement.setSelectionRange(value.length, value.length);
        }
    }

    private _formatString(value: string, pattern: string, mask?: string): string {
        const strippedValue = value.replace(/[^0-9]/g, "");
        const chars = strippedValue.split('');
        let count = 0;

        let formatted = '';
        for (let i = 0; i<pattern.length; i++) {
            const c = pattern[i];
            if (chars[count]) {
            if (/\*/.test(c)) {
                formatted += chars[count];
                count++;
            } else {
                formatted += c;
            }
            } else if (mask) {
            if (mask.split('')[i])
                formatted += mask.split('')[i];
            }
        }
        return formatted;
    }

    set formGroup(fg: FormGroup) {
        this._formGroup = fg;
    }

    get formGroup(): FormGroup {
        return this._formGroup;
    }

    private _setDayView(date: Date): void {
        this._date = date;
        this._setWeekDays();
        this._setDays();
    }

    private _setYearView(date: Date): void {
        const cols = 4;
        const rows = 6;
        const startYear = date.getFullYear() - 5;
        let value;

        for (let i = 0; i < rows; i++) {
            this.rows[i] = [];
            for (let j = 0; j < cols; j++) {
                value = startYear + i * cols + j;
                this.rows[i].push({value, compareValue: this._getYearCompareValue(value)});
            }
        }
    }

    private _setMonthsView(date: Date): void {
        const cols = 4;
        const rows = 3;
        this._date = date;
        for (let i = 0; i < rows; i++) {
            this.rows[i] = [];
            for (let j = 0; j < cols; j++) {
                this.rows[i].push({value: i * cols + j, compareValue: this._getMonthsCompareValue(i * cols + j)});
            }
        }
    }

    private _setDays(): void {
        for (let i = 0; i < this.weeks; i++) this.rows[this.rows.length] = [];
        this._setStartWeek();
        this._setWeeksLeft();
    
    }
    
    private _setWeekDays(): void {
        const defaultStartDay = new Date(this._date.setDate(1)).getDay();
        this.startDay = defaultStartDay - this.getFirstDay() > 0 ? defaultStartDay - this.getFirstDay() : 7 + defaultStartDay - this.getFirstDay();
        this.totalDays = new Date(this._date.getFullYear(), this._date.getMonth() + 1, 0).getDate();
        this.weeks = Math.ceil((this.startDay + this.totalDays) / 7);
    }
    
    private _setStartWeek(): void {
        const firstEmptyWeekDays = this.startDay;
        const firstWeekDays = 7 - this.startDay;
        if (this.startDay !== 0) {
            for (let i = 0; i < firstEmptyWeekDays; i++) this.rows[0].push({value: 0, compareValue: 0});
        }
        for (let i = 0; i < firstWeekDays; i++) this.rows[0].push({value: i + 1, compareValue: this._getCompareValue(i)});
    }
    
    private _setWeeksLeft(): void {
        const daysArr = new Array(this.totalDays);
        const first = 7 - this.startDay;
        for (let i = 1; i < this.weeks; i++) {
          for (let j = 0; j < daysArr.length; j++) {
            if (first + (7 * (i - 1)) < j + 1 && first + 7 * i > j) {
              this.rows[i].push({value: j + 1, compareValue: this._getCompareValue(j)});
            }
          }
        }
    }

    private _getCompareValue(i: number): number {
        return new Date(this._date.getFullYear(), this._date.getMonth(), i + 1).getTime();
    }

    private _getYearCompareValue(year: number): number {
        return new Date(year).getTime();
    }

    private _getMonthsCompareValue(month: number): number {
        return new Date(this._date.getFullYear(), month).getTime();
    }
}