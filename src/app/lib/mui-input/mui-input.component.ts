import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  forwardRef, HostListener,
  Input,
  Renderer2,
  ViewChild
} from '@angular/core';
import {ControlValueAccessor, FormControlName, NG_VALUE_ACCESSOR, NgControl} from "@angular/forms";
import {MuiLabelComponent} from "../mui-label/mui-label.component";
import {MuiIconsComponent} from "../mui-icons/mui-icons.component";
import {MuiInputDirective} from './mui-input.directive';

@Component({
  selector: 'mui-input',
  templateUrl: './mui-input.component.html',
  styleUrls: ['./mui-input.component.css']
})
export class MuiInputComponent implements AfterViewInit, ControlValueAccessor {

  @Input()
  set isDisabled(val: boolean) {
    this.setDisabled(val);
  }
  @Input() width = '300px';
  @Input() clearBtn = false;
  @ContentChild(MuiInputDirective, {read: ElementRef}) input!: ElementRef;
  @ViewChild('inputContainer') inputContainer!: ElementRef;
  @ContentChild(MuiLabelComponent, {read: ElementRef}) label: ElementRef | undefined;
  @ViewChild('labelContainer') labelContainer!: ElementRef;
  @ContentChild(MuiIconsComponent, {read: ElementRef}) icon: ElementRef | undefined;
  @ViewChild('iconContainer') iconContainer!: ElementRef;
  focus = false;
  set value(val: string) {
    this.setViewValue(val);
  }
  viewValue: string = '';
  isOpen = false;
  setValue: any;
  registerTouch: any;
  isFirstTouch = true;
  control: FormControlName;
  disabled = false;
  @HostListener('focusin') focusIn(): void {
    this.setFocus(true);
  }

  constructor(private renderer: Renderer2, private cd: ChangeDetectorRef, public ngControl: NgControl) {
    ngControl.valueAccessor = this;
    this.control = ngControl as FormControlName;
  }

  ngAfterViewInit(): void {
    if (this.label) {this.renderer.appendChild(this.labelContainer.nativeElement, this.label.nativeElement)}
    if (this.icon) {this.renderer.appendChild(this.iconContainer.nativeElement, this.icon.nativeElement)}
    if (this.input) {this.renderer.appendChild(this.inputContainer.nativeElement, this.input.nativeElement)}
    this.initInput();
    this.input.nativeElement.value = this.viewValue;
  }

  initInput(): void {
    this.setDisabled(this.disabled);
    this.renderer.addClass(this.input.nativeElement, 'mui-input');
  }

  setDisabled(val: boolean): void {
    this.disabled = val;
    if (!this.input) { return; }
    if (val) { this.renderer.setAttribute(this.input.nativeElement, 'disabled', ''); }
    else { this.renderer.removeAttribute(this.input.nativeElement, 'disabled', ''); }
  }

  setViewValue(val: any): void {
    this.viewValue = val;
    if (!this.input) { return; }
    this.input.nativeElement.value = val;
  }

  setFocus(status: any): void {
    if (this.disabled) {
      return;
    }
    this.focus = status;
    if (status) {
      setTimeout(() => this.input.nativeElement.focus());
      this.registerTouch();
    }
  }

  clearValue(): void {
    if (this.disabled) {
      return;
    }
    this.value = '';
    this.setValue(this.value);
  }

  updateValue(): void {
    this.value = this.input.nativeElement.value;
    this.setValue(this.viewValue);
  }

  onFocusOut(): void {
    this.isOpen = false;
  }

  registerOnChange(fn: any): void {
    this.setValue = fn;
  }

  registerOnTouched(fn: any): void {
    this.registerTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this.isOpen = false;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

}
