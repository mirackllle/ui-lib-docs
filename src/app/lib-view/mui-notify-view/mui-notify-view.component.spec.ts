import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiNotifyViewComponent } from './mui-notify-view.component';

describe('muiNotifyViewComponent', () => {
  let component: MuiNotifyViewComponent;
  let fixture: ComponentFixture<MuiNotifyViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiNotifyViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiNotifyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
