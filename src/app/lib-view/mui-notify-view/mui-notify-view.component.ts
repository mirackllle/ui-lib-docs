import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-notify-view',
  templateUrl: './mui-notify-view.component.html',
  styleUrls: ['./mui-notify-view.component.css']
})
export class MuiNotifyViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
