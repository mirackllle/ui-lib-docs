import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-mui-rangepicker-view',
  templateUrl: './mui-rangepicker-view.component.html',
  styleUrls: ['./mui-rangepicker-view.component.css']
})
export class MuiRangepickerViewComponent implements OnInit {

  tabNumber = 0;
  formGroup = new FormGroup({
    dateFrom: new FormControl(new Date(2021, 11, 20)),
    dateTo: new FormControl(new Date(2021, 11, 23))
  })
  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
