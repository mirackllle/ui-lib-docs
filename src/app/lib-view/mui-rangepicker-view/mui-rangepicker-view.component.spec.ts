import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiRangepickerViewComponent } from './mui-rangepicker-view.component';

describe('MuiRangepickerViewComponent', () => {
  let component: MuiRangepickerViewComponent;
  let fixture: ComponentFixture<MuiRangepickerViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiRangepickerViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiRangepickerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
