import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-button-nav-view',
  templateUrl: './mui-button-nav-view.component.html',
  styleUrls: ['./mui-button-nav-view.component.css']
})
export class MuiButtonNavViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
