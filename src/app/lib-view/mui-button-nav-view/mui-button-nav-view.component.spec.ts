import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonNavViewComponent } from './mui-button-nav-view.component';

describe('muiButtonNavViewComponent', () => {
  let component: MuiButtonNavViewComponent;
  let fixture: ComponentFixture<MuiButtonNavViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiButtonNavViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonNavViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
