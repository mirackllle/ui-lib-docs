import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-select-view',
  templateUrl: './mui-select-view.component.html',
  styleUrls: ['./mui-select-view.component.css']
})
export class MuiSelectViewComponent implements OnInit {

  tabNumber = 0;
  value = '';

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
