import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiSelectViewComponent } from './mui-select-view.component';

describe('muiSelectViewComponent', () => {
  let component: MuiSelectViewComponent;
  let fixture: ComponentFixture<MuiSelectViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiSelectViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiSelectViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
