import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiTabSwitcherViewComponent } from './mui-tab-switcher-view.component';

describe('muiTabSwitcherViewComponent', () => {
  let component: MuiTabSwitcherViewComponent;
  let fixture: ComponentFixture<MuiTabSwitcherViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiTabSwitcherViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiTabSwitcherViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
