import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-tab-switcher-view',
  templateUrl: './mui-tab-switcher-view.component.html',
  styleUrls: ['./mui-tab-switcher-view.component.css']
})
export class MuiTabSwitcherViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
