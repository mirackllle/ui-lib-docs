import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonViewComponent } from './mui-button-view.component';

describe('muiButtonViewComponent', () => {
  let component: MuiButtonViewComponent;
  let fixture: ComponentFixture<MuiButtonViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiButtonViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
