import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-button-view',
  templateUrl: './mui-button-view.component.html',
  styleUrls: ['./mui-button-view.component.css']
})
export class MuiButtonViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
