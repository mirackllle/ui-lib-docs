import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-button-sec-view',
  templateUrl: './mui-button-sec-view.component.html',
  styleUrls: ['./mui-button-sec-view.component.css']
})
export class MuiButtonSecViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
