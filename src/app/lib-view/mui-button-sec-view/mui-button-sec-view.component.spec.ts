import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonSecViewComponent } from './mui-button-sec-view.component';

describe('muiButtonSecViewComponent', () => {
  let component: MuiButtonSecViewComponent;
  let fixture: ComponentFixture<MuiButtonSecViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiButtonSecViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonSecViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
