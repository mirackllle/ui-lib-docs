import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-button-sec-nav-view',
  templateUrl: './mui-button-sec-nav-view.component.html',
  styleUrls: ['./mui-button-sec-nav-view.component.css']
})
export class MuiButtonSecNavViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
