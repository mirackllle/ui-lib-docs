import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiButtonSecNavViewComponent } from './mui-button-sec-nav-view.component';

describe('muiButtonSecNavViewComponent', () => {
  let component: MuiButtonSecNavViewComponent;
  let fixture: ComponentFixture<MuiButtonSecNavViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiButtonSecNavViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiButtonSecNavViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
