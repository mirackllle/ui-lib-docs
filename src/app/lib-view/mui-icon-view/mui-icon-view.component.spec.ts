import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiIconViewComponent } from './mui-icon-view.component';

describe('muiIconViewComponent', () => {
  let component: MuiIconViewComponent;
  let fixture: ComponentFixture<MuiIconViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiIconViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiIconViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
