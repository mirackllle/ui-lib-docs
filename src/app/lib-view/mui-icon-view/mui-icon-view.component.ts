import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-icon-view',
  templateUrl: './mui-icon-view.component.html',
  styleUrls: ['./mui-icon-view.component.css']
})
export class MuiIconViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
