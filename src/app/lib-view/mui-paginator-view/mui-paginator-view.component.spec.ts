import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiPaginatorViewComponent } from './mui-paginator-view.component';

describe('muiPaginatorViewComponent', () => {
  let component: MuiPaginatorViewComponent;
  let fixture: ComponentFixture<MuiPaginatorViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiPaginatorViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiPaginatorViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
