import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-paginator-view',
  templateUrl: './mui-paginator-view.component.html',
  styleUrls: ['./mui-paginator-view.component.css']
})
export class MuiPaginatorViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 
}
