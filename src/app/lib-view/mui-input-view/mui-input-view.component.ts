import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-input-view',
  templateUrl: './mui-input-view.component.html',
  styleUrls: ['./mui-input-view.component.css']
})
export class MuiInputViewComponent implements OnInit {

  tabNumber = 0;
  value = 'test value';

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
