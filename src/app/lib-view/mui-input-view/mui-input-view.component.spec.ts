import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiInputViewComponent } from './mui-input-view.component';

describe('muiInputViewComponent', () => {
  let component: MuiInputViewComponent;
  let fixture: ComponentFixture<MuiInputViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiInputViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiInputViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
