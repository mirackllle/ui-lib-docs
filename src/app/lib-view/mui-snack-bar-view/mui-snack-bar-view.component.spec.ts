import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiSnackBarViewComponent } from './mui-snack-bar-view.component';

describe('muiSnackBarViewComponent', () => {
  let component: MuiSnackBarViewComponent;
  let fixture: ComponentFixture<MuiSnackBarViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiSnackBarViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiSnackBarViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
