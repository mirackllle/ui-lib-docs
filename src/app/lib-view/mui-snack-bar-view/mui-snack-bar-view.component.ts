import { Component, OnInit } from '@angular/core';
import {MuiSnackBarService} from "../../lib/service/mui-snack-bar.service";

@Component({
  selector: 'app-mui-snack-bar-view',
  templateUrl: './mui-snack-bar-view.component.html',
  styleUrls: ['./mui-snack-bar-view.component.css']
})
export class MuiSnackBarViewComponent implements OnInit {

  tabNumber = 0;

  constructor(private snackBar: MuiSnackBarService) { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  }

  showSnackBarError() {
    this.snackBar.openErrorSnackBar('Test error massage', 5000, true);
  }

  showSnackBarSuccess() {
    this.snackBar.openSuccessSnackBar('Test success massage', 5000, true);
  }

  showSnackBarInfo() {
    this.snackBar.openInfoSnackBar('Test error massage', 5000, true);
  }

}
