import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiCheckboxViewComponent } from './mui-checkbox-view.component';

describe('muiCheckboxViewComponent', () => {
  let component: MuiCheckboxViewComponent;
  let fixture: ComponentFixture<MuiCheckboxViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiCheckboxViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiCheckboxViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
