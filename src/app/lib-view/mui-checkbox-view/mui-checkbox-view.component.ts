import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-checkbox-view',
  templateUrl: './mui-checkbox-view.component.html',
  styleUrls: ['./mui-checkbox-view.component.css']
})
export class MuiCheckboxViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
