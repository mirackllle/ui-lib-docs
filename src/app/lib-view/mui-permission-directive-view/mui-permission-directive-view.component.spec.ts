import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuiPermissionDirectiveViewComponent } from './mui-permission-directive-view.component';

describe('muiPermissionDirectiveViewComponent', () => {
  let component: MuiPermissionDirectiveViewComponent;
  let fixture: ComponentFixture<MuiPermissionDirectiveViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuiPermissionDirectiveViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuiPermissionDirectiveViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
