import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mui-permission-directive-view',
  templateUrl: './mui-permission-directive-view.component.html',
  styleUrls: ['./mui-permission-directive-view.component.css']
})
export class MuiPermissionDirectiveViewComponent implements OnInit {

  tabNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  tabChanged(i: number) {
    this.tabNumber = i;
  } 

}
